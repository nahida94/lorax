package encryptor

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type EncodedData struct {
	Data []string `json:data`
}

func CallEncoderClient(randomStrings interface{}, clientURL string) (*EncodedData, error) {
	reqBody, err := json.Marshal(randomStrings)
	if err != nil {
		return nil, err
	}

	resp, err := http.Post(clientURL, "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data := &EncodedData{}
	err = json.NewDecoder(resp.Body).Decode(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
