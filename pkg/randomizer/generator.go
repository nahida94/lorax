package randomizer

import (
	"math/rand"
	"sync"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

//AppendRandomString generate random string and append it to main slice.
//this function will generate string with different length
func GenerateRandomString(wg *sync.WaitGroup, jobs <-chan uint8, results chan<- string) {

	defer wg.Done()

	for range jobs {
		//rand.Intn(n) returns number in [0,n) .For preventing to generate empty strings
		//we just add 1 to the result of rand.Intn(n)
		b := make([]byte, rand.Intn(10)+1)

		for i := range b {
			b[i] = charset[rand.Intn(len(charset))]
		}

		results <- string(b)
	}

}
