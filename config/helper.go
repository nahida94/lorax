package config

import (
	"log"
	"os"
	"strconv"
)

func stringToInt(envName string, defaultValue int) int {
	intValue, err := strconv.Atoi(os.Getenv(envName))
	if err != nil {
		intValue = defaultValue
		log.Printf("%s is not set. default value %d: %+v\n", envName, intValue, err)
	}

	return intValue
}
