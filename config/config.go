package config

import (
	"log"
	"os"
)

const (
	defaultWorkersCount = 4
)

//Config holds all environment variables
type Config struct {
	Port               string
	ClientHost         string
	RandomStringLength int
	WorkersCount       int
}

//New read all environment variables with os package
//and set this values or default values to config instance
func New() *Config {
	clientHost := os.Getenv("CLIENT_HOST")
	if clientHost == "" {
		log.Fatal("client host does not provided")
	}

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("service port does not provided")
	}

	return &Config{
		Port:         port,
		ClientHost:   clientHost,
		WorkersCount: stringToInt("WORKERS_COUNT", defaultWorkersCount),
	}
}
