package main

import (
	"context"
	"gitlab.com/sosiv/lorax/api/server"
	"gitlab.com/sosiv/lorax/config"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	cfg := config.New()
	customHandler := server.NewHandler(ctx, cfg)

	// Handle sigterm and await termChan signal
	termChan := make(chan os.Signal)
	signal.Notify(termChan, syscall.SIGTERM, syscall.SIGINT)

	//create new web server and run using goroutine
	srv := server.New(customHandler, cfg.Port)
	go func() {
		log.Printf("Server is ready to handle requests at :%s", cfg.Port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Could not listen on %s: %v\n", cfg.Port, err)
		}
	}()

	// Wait for SIGTERM to be captured
	<-termChan
	log.Println("Server is shutting down...")

	// Shutdown the HTTP server
	srv.SetKeepAlivesEnabled(false)
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
	}

	// Cancel the context, this will make the handlers stop
	cancel()

	log.Println("waiting for running jobs to finish")
	log.Println("jobs finished. exiting")

}
