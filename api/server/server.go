package server

import (
	"net/http"
)

func New(h *handler, port string) *http.Server {
	router := http.NewServeMux()
	router.HandleFunc("/api/v1/encryption_scopes/", h.getEncryptedStrings)

	return &http.Server{
		Addr:    ":" + port,
		Handler: router,
	}
}
