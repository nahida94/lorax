package server

import (
	"context"
	"encoding/json"
	"gitlab.com/sosiv/lorax/client/encryptor"
	"gitlab.com/sosiv/lorax/config"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// Our custom handler that holds a wait group used to block the shutdown
type handler struct {
	ctx context.Context
	cfg *config.Config
}

func NewHandler(ctx context.Context, cfg *config.Config) *handler {
	return &handler{
		ctx: ctx,
		cfg: cfg,
	}
}

type RandomStringData struct {
	Data []string `json:"data"`
}

func (h *handler) getEncryptedStrings(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		http.Error(w, "not found", http.StatusNotFound)
		return
	}

	numJobs, err := strconv.Atoi(strings.TrimPrefix(r.URL.Path, "/api/v1/encryption_scopes/"))
	if err != nil {
		log.Printf("path param did not define. %+v", err)
		http.Error(w, "path param did not define", http.StatusBadRequest)
		return
	}

	//this function call random string generator function async
	//listen to os SIGTERM signal
	randomStrings, err := CallRandomizerAsync(h.ctx, numJobs, h.cfg.WorkersCount)
	if err != nil {
		log.Println(err)
		http.Error(w, "something went wrong", http.StatusInternalServerError)
		return
	}

	//call encoder service client
	encryptedStrings, err := encryptor.CallEncoderClient(randomStrings, h.cfg.ClientHost)
	if err != nil {
		log.Printf("unable to get response from encoder client. %+v", err)
		http.Error(w, "something went wrong", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(encryptedStrings)
}
