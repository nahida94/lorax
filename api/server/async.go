package server

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/sosiv/lorax/pkg/randomizer"
)

func CallRandomizerAsync(ctx context.Context, numJobs int, workersCount int) (*RandomStringData, error) {
	var wg sync.WaitGroup

	jobs := make(chan uint8, numJobs)
	results := make(chan string, numJobs)

	randomStrings := &RandomStringData{
		Data: make([]string, numJobs),
	}

	for i := 0; i < workersCount; i++ {
		select {
		// If the context was cancelled, a SIGTERM was captured
		case <-ctx.Done():
			wg.Wait()
			return nil, fmt.Errorf("context was cancelled")
		default:
			wg.Add(1)
			go randomizer.GenerateRandomString(&wg, jobs, results)
		}
	}

	for i := 0; i < numJobs; i++ {
		jobs <- 1
	}
	close(jobs)

	for i := 0; i < numJobs; i++ {
		randomStrings.Data[i] = <-results
	}

	//wait for all goroutines
	wg.Wait()

	return randomStrings, nil
}
