#### String randomizer RESTFUL service

This service will receive a number of wanted strings and create that number of random
strings and send them to the string encryptor service.
It will return the encrypted random string to the user.

The project layouts designed based this guideline:
[project-layout](https://github.com/golang-standards/project-layout)

## Usage

##### run project in local
move cdm/lorax folder <br />
```cd cmd/lorax```

build project <br />
```go build```

define required environment variables
```CLIENT_HOST=localhost:8082``` <br />
```PORT=8081```

##### run in docker 
build an image from a Dockerfile <br />
```docker build -t lorax . ``` <br />

run docker container <br />
``` docker run --env CLIENT_HOST=http://localhost:8082/api/v1/encryptor_scopes/ --env PORT=8081 -p 8081:8081 lorax```

#### get the result
You just need to send GET request to this URI<br />. This api is public
```http://localhost:8081/api/v1/encryption_scopes/:string_count``` <br />
URL examples:<br /> 
```http://localhost:8081/api/v1/encryption_scopes/100``` <br />
```http://localhost:8081/api/v1/encryption_scopes/35```


### env variables
```CLIENT_HOST``` required <br />
```PORT``` required <br/>
```WORKERS_COUNT``` default value is 4
